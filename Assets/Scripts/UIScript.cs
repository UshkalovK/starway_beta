﻿#pragma warning disable 0649

using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIScript : MonoBehaviour
{
    [SerializeField]
    private UnityEngine.UI.Text ScoreLabel;
    [SerializeField]
    private UnityEngine.UI.Text HighscoreLabel;
    [SerializeField]
    private UnityEngine.UI.Text TimeLabel;
    [SerializeField]
    private UnityEngine.UI.Text AsteroidsLabel;
    [SerializeField]
    private UnityEngine.UI.Text EndHighscore;
    [SerializeField]
    private UnityEngine.UI.Text EndTime;
    [SerializeField]
    private UnityEngine.UI.Text EndAsteroids;
    [SerializeField]
    private GameObject Congrats;
    [SerializeField]
    private GameObject GameOverPanel;
    [SerializeField]
    private GameObject GamePanel;
    [SerializeField]
    private GameObject StartPanel;
    [SerializeField]
    private GameObject PausePanel;

    private int highscore;
    private int secs = 0;
    private int mins = 0;
    private float tmpsecs = 0;
    private bool isHighscoreBeat = false;
    
    public int asteroids = 0;
    public int deltaScore = 1;
    public int score = 0;

    public static UIScript instance;

    void Start()
    {
        highscore = PlayerPrefs.GetInt("Highscore");
        instance = this;
    }

    void Update()
    {
        if (PlayerScript.instance.isStart)
        {
            //Start panel disable and starting the game
            if (Input.anyKey)
            {
                StartPanel.SetActive(false);
                GamePanel.SetActive(true);
                PlayerScript.instance.isStart = false;
                PlayerScript.instance.isPlaying = true;
            }
        }
        if (PlayerScript.instance.isPlaying && !PlayerScript.instance.isStart)
        {
            //Timer
            tmpsecs += Time.deltaTime;
            if (tmpsecs >= 1f)
            {
                ++secs;
                score += deltaScore;
                tmpsecs = 0;
            }
            if (secs >= 60)
            {
                secs = 0;
                ++mins;
            }
            if (score > highscore)
            {
                highscore = score;
                isHighscoreBeat = true;
            }
            //Show stats in the game
            TimeLabel.text = "Time: " + mins + " min " + secs + " sec";
            ScoreLabel.text = "Score: " + score;
            HighscoreLabel.text = "Highscore: " + highscore;
            AsteroidsLabel.text = "Asteroids: " + asteroids;

            //Open Pause menu by escape click
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PausePanel.SetActive(true);
                Time.timeScale = 0;
            }
        }
        else if(!PlayerScript.instance.isStart)
        {
            //Show Game over panel with total stats
            PlayerPrefs.SetInt("Highscore", highscore);
            PlayerPrefs.Save();
            EndAsteroids.text = "Asteroids passed: " + asteroids;
            EndHighscore.text = "Total score: " + score;
            EndTime.text = TimeLabel.text;
            GameOverPanel.SetActive(true);
            GamePanel.SetActive(false);
            if (isHighscoreBeat) Congrats.SetActive(true);
        }
        
    }

    /// <summary>
    ///     On button "Reset" reload scene
    /// </summary>
    public void OnReset()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }

    /// <summary>
    ///     On button "Exit" exit from game
    /// </summary>
    public void OnExit()
    {
        Application.Quit();
    }

    /// <summary>
    ///     On button "Play" resume the game in Pause menu 
    /// </summary>
    public void OnResume()
    {
        PausePanel.SetActive(false);
        Time.timeScale = 1;
    }
   
}
