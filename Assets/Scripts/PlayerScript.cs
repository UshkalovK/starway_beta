﻿#pragma warning disable 0649

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class PlayerScript : MonoBehaviour
{
    [SerializeField]
    private float xMin, xMax;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float tilt;

    private Rigidbody Ship;

    public bool isPlaying = false;
    public bool isStart = true;

    private const string COLLISION_TAG_NAME = "Asteroid";
    
    public static PlayerScript instance;
    void Start()
    {
        Ship = GetComponent<Rigidbody>();
        instance = this;

    }

    void Update()
    {
        if (isPlaying && !isStart)
        {
            //Moving player left-right within the boundaries of the road
            float moveX = Input.GetAxis("Horizontal");
            Ship.velocity = new Vector3(moveX, 0, 0) * speed;
            float clampedX = Mathf.Clamp(Ship.position.x, xMin, xMax);
            Ship.position = new Vector3(clampedX, Ship.position.y, Ship.position.z);

            //Ship rotation
            Ship.rotation = Quaternion.Slerp(Quaternion.identity, Quaternion.Euler(0, 0, -moveX * tilt), 0.7f);

            //Boost!
            if (Input.GetKeyDown(KeyCode.Space))
            {
                speed += 10;
                tilt += 25;
                ScrollerScript.instance.speed *= 2f;
                AsteroidScript.instance.changeSpeed = 2f;
                SmoothFollow.instance.wantedDistance = 2.34f;
                SmoothFollow.instance.height = 0.85f;
                UIScript.instance.deltaScore = 2;

            }
            else if (Input.GetKeyUp(KeyCode.Space))
            {
                speed -= 10;
                tilt -= 25;
                ScrollerScript.instance.speed /= 2;
                AsteroidScript.instance.changeSpeed = 1f;
                SmoothFollow.instance.wantedDistance = 4.14f;
                SmoothFollow.instance.height = 1.71f;
                UIScript.instance.deltaScore = 1;
            }
        }
        
    }

    //Collision with the asteroid
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == COLLISION_TAG_NAME)
        {
            Destroy(gameObject);
            isPlaying = false;
        }
    }

}
