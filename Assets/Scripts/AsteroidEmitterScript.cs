﻿#pragma warning disable 0649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidEmitterScript : MonoBehaviour
{
    [SerializeField]
    private GameObject asteroid;
    [SerializeField]
    private float minDelay, maxDelay;

    private float nextLaunchTime = 0;

    public static AsteroidEmitterScript instance;
    void Start()
    {
        instance = this;
    }

    void Update()
    {

        //Emitt asteroids with delay
        if (Time.time > nextLaunchTime && PlayerScript.instance.isPlaying && !PlayerScript.instance.isStart)
        {
            float emitterSize = transform.localScale.x;
            GameObject newAsteroid = Instantiate(asteroid, new Vector3(Random.Range(-emitterSize / 2, emitterSize / 2), transform.position.y, transform.position.z), Quaternion.identity);
            newAsteroid.transform.localScale *= Random.Range(0.3f, 0.6f);

            //Asteroids will emitt more often during the game
            if (minDelay > 0.05f) minDelay -= 0.02f;
            if (maxDelay > 0.5f) maxDelay -= 0.02f;

            nextLaunchTime = Time.time + Random.Range(minDelay, maxDelay);
        }
    }
}
