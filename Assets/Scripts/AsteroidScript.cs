﻿#pragma warning disable 0649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidScript : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed;

    public float minSpeed, maxSpeed;
    public float changeSpeed;
    public bool isPlaying = true;

    public static AsteroidScript instance;
    void Start()
    {
        instance = this;
        Rigidbody asteroid = GetComponent<Rigidbody>();

        //Increasing asteroids speed during the game
        if (minSpeed < 50) minSpeed += 0.2f;
        if (maxSpeed < 85) maxSpeed += 0.2f;

        //Asteroids rotation around the Y-axis whith random speed
        asteroid.angularVelocity = new Vector3(0, Random.insideUnitSphere.y * rotationSpeed, 0);

        //Random speed of asteroids
        if (PlayerScript.instance.isPlaying)
            asteroid.velocity = new Vector3(0, 0, -Random.Range(minSpeed, maxSpeed) * changeSpeed) ;
    }

    private void Update()
    {
        //Stopping asteroids when player is dead
        if (!PlayerScript.instance.isPlaying || PlayerScript.instance.isStart)
        {
            Rigidbody asteroid = GetComponent<Rigidbody>();
            asteroid.velocity = new Vector3(0, 0, 0);
        }
    }
}
