﻿using UnityEngine;

public class SmoothFollow : MonoBehaviour
{
    public float distance = 34.64f;
    public float height = 12.1f;
    public float heightDamping = 2.0f;
    public float distanceDamping = 2.0f;
    public float wantedDistance = 4.14f;

    public float rotationDamping = 3.0f;
    public Transform target;

    public static SmoothFollow instance;

    private void Start()
    {
        instance = this;
    }
    private void LateUpdate()
    {
        // Early out if we don't have a target
        if (!target)
        {
            return;
        }

        // Calculate the current rotation angles
        float wantedRotationAngle = target.eulerAngles.y;
        float wantedHeight = target.position.y + height;

        float currentRotationAngle = transform.eulerAngles.y;
        float currentHeight = transform.position.y;
        float currentDistance = distance;

        // Damp the rotation around the y-axis
        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

        // Damp the height
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

        // Damp the distance
        distance = Mathf.Lerp(currentDistance, wantedDistance, distanceDamping * Time.deltaTime);

        // Convert the angle into a rotation
        Quaternion currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

        // Set the position of the camera on the x-z plane to:
        // distance meters behind the target
        var pos = transform.position;
        pos = target.position - currentRotation * Vector3.forward * distance;
        pos.y = currentHeight;
        transform.position = pos;

        // Always look at the target
        transform.LookAt(target);
    }
}