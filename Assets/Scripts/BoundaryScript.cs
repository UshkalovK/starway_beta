﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryScript : MonoBehaviour
{
    private const string COLLISION_TAG_NAME = "Asteroid";

    //Destroy asteroids, when they pass over the player
    private void OnTriggerExit(Collider other)
    {
        if(other.tag == COLLISION_TAG_NAME)
            Destroy(other.gameObject);
    }

    //Counting asteroids, which player passed
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == COLLISION_TAG_NAME)
        {
            UIScript.instance.score += 5;
            UIScript.instance.asteroids += 1;

        }
    }
}
