﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollerScript : MonoBehaviour
{
    public float speed;
    Vector3 startPosition;

    public static ScrollerScript instance;
    void Start()
    {
        startPosition = transform.position;
        instance = this;
    }

    void Update()
    {
        //Imitation of movement. Road is moving to the certain point and then repeat from start
        float shift = Mathf.Repeat(Time.time * speed, 10f);

        if(PlayerScript.instance.isPlaying && !PlayerScript.instance.isStart)
            transform.position = startPosition + new Vector3(0, 0, -shift);
    }
}
