﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetScript : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed;

    void Start()
    {
        //Rotation of the planet over the Y-axis
        Rigidbody asteroid = GetComponent<Rigidbody>();
        asteroid.angularVelocity = new Vector3(0, Random.insideUnitSphere.y * rotationSpeed, 0);
    }
}
